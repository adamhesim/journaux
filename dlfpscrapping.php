#!/usr/bin/php
<?php

function getPageContent($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function parseJournauxContent($html) {
    $dom = new DOMDocument;
    @$dom->loadHTML($html);
    $xpath = new DOMXPath($dom);
    $journals = [];

    foreach ($xpath->query('//article[contains(@class, "diary")]') as $article) {
        $urlNode = $xpath->query('.//footer//a[@itemprop="url"]', $article)->item(0);
        $scoreNode = $xpath->query('.//figure[@class="score"]', $article)->item(0);
        $commentsNode = $xpath->query('.//footer//span[@class="nb_comments"]', $article)->item(0);

        if ($urlNode && $scoreNode && $commentsNode) {
            $journal = [
                'url' => 'https://linuxfr.org' . $urlNode->getAttribute('href'),
                'score' => intval(trim($scoreNode->nodeValue)),
                'nbcomments' => intval(preg_replace('/[^\d]/', '', $commentsNode->nodeValue))
            ];
            $journals[] = $journal;
        }
    }

    return $journals;
}
function parseLiensContent($html) {
    $dom = new DOMDocument;
    @$dom->loadHTML($html);
    $xpath = new DOMXPath($dom);
    $journals = [];

    foreach ($xpath->query('//article[contains(@class, "node hentry")]') as $article) {
        // Extract the URL from the 'href' attribute of the second <a> tag inside the <h1> tag.
        $urlNode = $xpath->query('.//h1[@class="entry-title"]/a[2]', $article)->item(0);

        // Extract the score from the <figure> tag with class 'score'.
        $scoreNode = $xpath->query('.//figure[@class="score"]', $article)->item(0);

        // Extract the number of comments from the content of the <span> tag with class 'nb_comments'.
        $commentsNode = $xpath->query('.//footer//span[contains(@class, "nb_comments")]', $article)->item(0);

        if ($urlNode && $scoreNode && $commentsNode) {
            $journal = [
                'url' => 'https://linuxfr.org' . $urlNode->getAttribute('href'),
                'score' => intval(trim($scoreNode->nodeValue)),
                'nbcomments' => intval(preg_replace('/[^\d]/', '', $commentsNode->nodeValue))
            ];
            $journals[] = $journal;
        }
    }

    return $journals;
}
function processAndSave($type) {
    $baseUrl = "https://linuxfr.org/$type";
    $outputFile = "public/dlfp_$type.json";
    $parserFunction = "parse" . ucfirst($type) . "Content";

    $results = [];
    for ($i = 1; $i <= 20; $i++) {
        $url = $baseUrl . "?page=$i";
        echo "\n$url\n";
        $pageContent = getPageContent($url);
        $results = array_merge($results, $parserFunction($pageContent));
        echo count($results)."\n";
        sleep(2);
    }
    file_put_contents($outputFile, json_encode($results, JSON_PRETTY_PRINT));
}

// Process Journaux
processAndSave('journaux');

// Process Liens
processAndSave('liens');